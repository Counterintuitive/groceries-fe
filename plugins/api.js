import apiFactory from "../repository/index";

export default ( { $axios}, inject ) => {
  const api = apiFactory($axios)
  inject('api', api)
}
