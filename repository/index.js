import RecipeRepository from './recipes/RecipeRepository'

function apiFactory(axios) {
  return {
    recipes: RecipeRepository(axios)
  }
}

export default apiFactory
