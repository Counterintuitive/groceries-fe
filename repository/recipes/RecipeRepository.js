const RecipeRepository = axios => {
  return {
    getRecipes(params) {
      return axios.$get('/recipes', { params })
    },
  }
}

export default RecipeRepository
