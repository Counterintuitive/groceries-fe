export const TYPES = {
  RECIPES_SUCCESS: 'RECIPES_SUCCESS'
}

export const state = () => ({
  recipes: [],
  ingredients: []
})

export const getters = {
  recipes: state => state.recipes,
  ingredients: state => state.ingredients
}

export const actions = {
  getRecipes({ commit, state }, params) {

    return this.$api.recipes.getRecipes(params)
    .then(response => {
      commit(TYPES.RECIPES_SUCCESS, response)

      return response
    })
    .catch(errors => {
      console.log(errors)
    })
  },
}

export const mutations = {
  [TYPES.RECIPES_SUCCESS](state, data) {
    state.recipes = data
  }
}
